#!/bin/sh

HOUND_DRIVER=phantomjs
HOUND_HOST=phantomjs
HOUND_PORT=8910
HOUND_BROWSER=

TEST_APP_HOST=http://bdd_demo_app
TEST_APP_PORT=4001

docker-compose run \
  -e MIX_ENV=test \
  -e HOUND_DRIVER=$HOUND_DRIVER \
  -e HOUND_HOST=$HOUND_HOST \
  -e HOUND_PORT=$HOUND_PORT \
  -e HOUND_BROWSER=$HOUND_BROWSER \
  -e TEST_APP_HOST=$TEST_APP_HOST \
  -e TEST_APP_PORT=$TEST_APP_PORT \
  --name bdd_demo_app \
  --service-ports \
  --use-aliases \
  bdd_demo_app mix test

docker-compose down
