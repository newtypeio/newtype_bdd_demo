Feature: 用户注册
  作为一个想要进行投资的新用户
  我想要 注册账户
  以便 我能够开展投资

  Scenario: 成功注册
    # 通过有效的用户名和密码正常注册
    Given 我 "edwardzhou" 尚未注册过账号
    And 未登录系统
    And 我在 "首页"
    When 我点击 "注册"
    And 我应当在 "注册页面"
    And 我在 "用户名" 输入 "edwardzhou"
    And 我在 "密码" 输入 "password"
    And 我在 "密码确认" 输入 "password"
    And 我点击 "#btn_sign_up"
    Then 我应当看到 "Welcome edwardzhou"

  Scenario: 密码不匹配
    # 两次输入的密码不匹配, 提示错误信息
    Given 我 "edwardzhou" 尚未注册过账号
    And 未登录系统
    And 我在 "首页"
    When 我点击 "注册"
    And 我应当在 "注册页面"
    And 我在 "用户名" 输入 "edwardzhou"
    And 我在 "密码" 输入 "password"
    And 我在 "密码确认" 输入 "passwxxd"
    And 我点击 "#btn_sign_up"
    Then 我应当在 "注册页面"
    And 我应当看到 "密码不一致"

  Scenario: 用户名长度小于8位
    # 两次输入的密码不匹配, 提示错误信息
    Given 我 "edward" 尚未注册过账号
    And 未登录系统
    And 我在 "首页"
    When 我点击 "注册"
    And 我应当在 "注册页面"
    And 我在 "用户名" 输入 "edward"
    And 我在 "密码" 输入 "password"
    And 我在 "密码确认" 输入 "password"
    And 我点击 "#btn_sign_up"
    Then 我应当在 "注册页面"
    And 我应当看到 "用户名不能少于8个字符"

  Scenario: 用户名没有填写
    # 两次输入的密码不匹配, 提示错误信息
    Given 我 "edward" 尚未注册过账号
    And 未登录系统
    And 我在 "首页"
    When 我点击 "注册"
    And 我应当在 "注册页面"
    And 我在 "用户名" 输入 ""
    And 我在 "密码" 输入 "password"
    And 我在 "密码确认" 输入 "password"
    And 我点击 "#btn_sign_up"
    Then 我应当在 "注册页面"
    And 我应当看到 "请填写用户名"
