defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers

  scenario_starting_state fn _state ->
    Application.ensure_all_started(:hound)
    Hound.start_session
  end
  
  scenario_finalize fn _state ->
    Hound.end_session
  end

  given_ ~r/^我 "(?<username>[^"]+)" 尚未注册过账号$/,
  fn state, %{username: _username} ->
    {:ok, state}
  end

  and_ ~r/^未登录系统$/, fn state ->
    {:ok, state}
  end

  and_ ~r/^我在 "(?<inputbox>[^"]+)" 输入 "(?<value>[^"]+)"$/,
  fn state, %{inputbox: inputbox, value: value} ->
    inputbox
    |> inputbox_id()
    |> element_by_id()
    |> fill_field(value)

    {:ok, state}
  end

  and_ ~r/^我点击 "(?<button>[^"]+)"$/,
  fn state, %{button: button} ->
    button
    |> button_id()
    |> element_by_id()
    |> click()

    {:ok, state}
  end

  then_ ~r/^我应当看到 "(?<text>[^"]+)"$/,
  fn state, %{text: text} ->

    assert visible_page_text() =~ text

    {:ok, state}
  end
  
  and_ ~r/^我在 "(?<page>[^"]+)"$/,
  fn state, %{page: page} ->
    page
    |> page_path()
    |> navigate_to()

    {:ok, state}
  end
  
  and_ ~r/^我应当在 "(?<page>[^"]+)"$/,
  fn state, %{page: page} ->
    assert current_page_id() == page_id(page)

    {:ok, state}
  end

  and_ ~r/^暂停 "(?<seconds>[^"]+)" 秒$/,
  fn state, %{seconds: seconds} ->
    seconds
    |> String.to_integer
    |> Kernel.*(1000)
    |> :timer.sleep()

    {:ok, state}
  end

  defp element_by_id(id) do
    find_element(:id, id)
  end

  defp page_path("注册页面"), do: "/sign_up"
  defp page_path("首页"), do: "/"

  defp page_id("注册页面"), do: "sign_up_page"
  defp page_id("首页"), do: "home_page"

  defp current_page_id do
    attribute_value({:id, "current_page"}, "page_id")
  end

  defp button_id("注册"), do: "btn_sign_up"
  defp button_id("#" <> id), do: id
  
  defp inputbox_id("用户名"), do: "username_field"
  defp inputbox_id("密码"), do: "password_field"
  defp inputbox_id("密码确认"), do: "password_confirmation_field"
end
