defmodule BddDemoWeb.Features.SignUpTest do
  use Cabbage.Feature, async: false, file: "sign_up.feature"
  use Hound.Helpers

  setup do
    on_exit fn ->
      Hound.end_session
    end

    Application.ensure_all_started(:hound)
    Hound.start_session

    :ok = Ecto.Adapters.SQL.Sandbox.checkout(BddDemo.Repo)
    # Setting the shared mode must be done only after checkout
    Ecto.Adapters.SQL.Sandbox.mode(BddDemo.Repo, {:shared, self()})
    
    %{}
  end

  defgiven ~r/^我 "(?<username>[^"]+)" 尚未注册过账号$/, %{username: _username}, state do
    {:ok, state}
  end

  defand ~r/^未登录系统$/, _vars, state do
    {:ok, state}
  end

  defand ~r/^我在 "(?<inputbox>[^"]+)" 输入 "(?<value>[^"]*)"( .)?$/, %{inputbox: inputbox, value: value}, state do
    inputbox
    |> inputbox_id()
    |> element_by_id()
    |> fill_field(value)

    {:ok, state}
  end

  defand ~r/^我点击 "(?<button>[^"]+)"$/, %{button: button}, state do
    button
    |> button_id()
    |> element_by_id()
    |> click()

    {:ok, state}
  end

  defand ~r/^我应当看到 "(?<text>[^"]+)"$/, %{text: text}, state do
    assert visible_page_text() =~ text

    {:ok, state}
  end
  
  defand ~r/^我在 "(?<page>[^"]+)"$/, %{page: page}, state do
    page
    |> page_path()
    |> navigate_to()

    {:ok, state}
  end
  
  defand ~r/^我应当在 "(?<page>[^"]+)"$/, %{page: page}, state do
    assert current_page_id() == page_id(page)

    {:ok, state}
  end

  defand ~r/^暂停 "(?<seconds>[^"]+)" 秒$/, %{seconds: seconds}, state do
    seconds
    |> String.to_integer
    |> Kernel.*(1000)
    |> :timer.sleep()

    {:ok, state}
  end

  defp element_by_id(id) do
    find_element(:id, id)
  end

  defp page_path("注册页面"), do: "/sign_up"
  defp page_path("首页"), do: "/"

  defp page_id("注册页面"), do: "sign_up_page"
  defp page_id("首页"), do: "home_page"

  defp current_page_id do
    attribute_value({:id, "current_page"}, "page_id")
  end

  defp button_id("注册"), do: "btn_sign_up"
  defp button_id("#" <> id), do: id
  
  defp inputbox_id("用户名"), do: "username_field"
  defp inputbox_id("密码"), do: "password_field"
  defp inputbox_id("密码确认"), do: "password_confirmation_field"

end
