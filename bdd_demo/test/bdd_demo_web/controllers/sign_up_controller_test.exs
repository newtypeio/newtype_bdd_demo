defmodule BddDemoWeb.SignUpControllerTest do
  use BddDemoWeb.ConnCase

  @signup_attrs %{
    username: "new_username",
    password: "password",
    password_confirmation: "password"
  }

  describe "new sign up" do
    test "render new form", %{conn: conn} do
      conn = get conn, sign_up_path(conn, :new)
      assert html_response(conn, 200) =~ "新用户注册"
    end      
  end

  describe "sign up" do
    test "redirect to home page when signed up", %{conn: conn} do
      conn = post conn, sign_up_path(conn, :create), user: @signup_attrs
      assert redirected_to(conn) =~ page_path(conn, :index)
      assert get_flash(conn, :info) == "Welcome new_username"
    end    
  end
end