defmodule BddDemo.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset


  schema "users" do
    field :password, :string
    field :username, :string

    field :password_confirmation, :string, virtual: true

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :password])
    |> validate_required([:username, :password])
  end

  @doc false
  def signup_changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :password, :password_confirmation])
    |> validate_required(:username, message: "请填写用户名")
    |> validate_required([:password, :password_confirmation])
    |> validate_length(:username, min: 8, message: "用户名不能少于8个字符")
    |> validate_confirmation(:password, message: "密码不一致")
  end
end
