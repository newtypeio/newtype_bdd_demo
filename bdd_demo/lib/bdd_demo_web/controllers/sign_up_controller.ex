defmodule BddDemoWeb.SignUpController do
  use BddDemoWeb, :controller

  alias BddDemo.Accounts
  alias BddDemo.Accounts.User

  def new(conn, _params) do
    changeset = Accounts.change_user(%User{})
    render conn, "new.html", changeset: changeset
  end

  def create(conn, %{"user" => user_params}) do
    case Accounts.signup_user(user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Welcome #{user.username}")
        |> redirect(to: page_path(conn, :index))
      {:error, changeset} ->
        conn
        |> render("new.html", changeset: changeset)
    end

  end

end