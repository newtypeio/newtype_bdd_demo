defmodule BddDemoWeb.Router do
  use BddDemoWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", BddDemoWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index

    resources "/sign_up", SignUpController, only: [:new, :create]
  end

  # Other scopes may use custom stacks.
  # scope "/api", BddDemoWeb do
  #   pipe_through :api
  # end
end
