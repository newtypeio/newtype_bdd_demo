use Mix.Config

# In this file, we keep production configuration that
# you'll likely want to automate and keep away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or yourself later on).
config :bdd_demo, BddDemoWeb.Endpoint,
  secret_key_base: "M7D6Z0+ljhwc7tsIakEtR+MLPZipXPG0VG1+3eVnYBcuif8Iq0JEvIEoKszy/TWm"

# Configure your database
config :bdd_demo, BddDemo.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "bdd_demo_prod",
  pool_size: 15
