use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :bdd_demo, BddDemoWeb.Endpoint,
  http: [port: 4001],
  server: true

# Print only warnings and errors during test
config :logger, level: :warn

config :hound, 
  driver: System.get_env("HOUND_DRIVER") || "phantomjs",
  host: System.get_env("HOUND_HOST") || "http://localhost",
  port: System.get_env("HOUND_PORT") || 8910
# config :hound, driver: "selenium"
# # Use Chrome with the default driver (selenium)
config :hound, browser: System.get_env("HOUND_BROWSER")
config :hound, 
  app_host: System.get_env("TEST_APP_HOST") || "http://localhost", 
  app_port: System.get_env("TEST_APP_PORT") || 4001
# #config :hound, driver: "selenium"


# Configure your database
config :bdd_demo, BddDemo.Repo,
  adapter: Ecto.Adapters.Postgres,
  hostname: System.get_env("TEST_DB_HOST") || "localhost",
  username: System.get_env("TEST_DB_USER") || "postgres",
  password: System.get_env("TEST_DB_PASSWORD") || "postgres",
  database: System.get_env("TEST_DB_NAME") || "bdd_demo_test",
  port: System.get_env("TEST_DB_PORT") || 5432,
  pool: Ecto.Adapters.SQL.Sandbox
