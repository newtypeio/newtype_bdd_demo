# BDD example

演示如何通过自动化验收标准实现 用户注册 功能

# HOW TO RUN

```bash
# clone 
git clone https://gitlab.com/edwardzhou/newtype_bdd_demo.git

# enter project folder
cd atfx_bdd_demo

# prepare .env
cp local.env .env

# run with docker-compose, supposed docker-compose is installed already in your PC.
docker-compose up --build

# 1. open WebBrowser eg. chrome, and visit http://localhost:4000/
# 2. click "注册新账号" in the page to sign up new account
# 3. you will get welcome message once signed up successfully


```

# HOW TO RUN TEST

## run test with default driver PhantomJS (without WEB UI)
```
# linux & mac
./run_phantomjs.sh

# windows (the script is not yet ready)
run_phantomjs.bat
```

## run test with Selenium (with chrome)
please download selenium-standalone from https://goo.gl/FCSwwD

run selenium and make sure chrome is installed
```
java -jar <path>/selenium-server-standalone-3.14.0.jar
```

```
# linux & mac
./run_local_selenium.sh

# windows (the script is not yet ready)
run_local_selenium.bat
```


# Reference
### Automatic Acceptance Criteria:

https://gitlab.com/edwardzhou/newtype_bdd_demo/blob/master/bdd_demo/test/features/sign_up.feature

### controller test case:
https://gitlab.com/edwardzhou/newtype_bdd_demo/blob/master/bdd_demo/test/bdd_demo_web/controllers/sign_up_controller_test.exs


# Have Fun

